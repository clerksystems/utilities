```
functions array.js
	flatten
	uniqueArray
	isArray
	inArray
	average
	forceArray
functions file.js
	readFile
	readDir
	isTrueDirEntry
	filterActualFiles
	filterActualDirectories
	getFullContentPaths
	getExistingFiles
	getExistingDirectories
	fileExistsSync
	fileExists
	directoryExists
	dirExists
	dirExistsSync
	readJsonFileIfExists
	createFileIfNotExistsSync
	createDirIfNotExistsSyncRecursivly
	createDirIfNotExistsSync
	createDirIfNotExists
	deleteFileIfExistsSync
	deleteFileIfExists
	rmdirRecursivelySync
	rmdirRecursively
	emptyDirectory
	humanizedFileSize
	getFileExtension
	getFileNameFromUrl
	createDirStructure
functions function.js
	promisify
	promiseToFnAll
	promiseToMapAll
	callerName
	getErrorObject
functions math.js
	log10
functions object.js
	clone
	objectSize
	forceTemplate
	mergeObjects
	identicalKeys
	typeOf
	surroTypeOf
	findEntry
	sortKeys
	getKeys
	addGetter
	forObject
functions string.js
	ucfirst
	isValidEmailAddress
	sortVersions
functions time.js
	timestamp
	humanizedTimestamp
	humanizedTimeSpan
```