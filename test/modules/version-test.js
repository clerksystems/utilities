'use strict'

const	{expect}			= require( 'chai' )

const	sortVersions		= require( '../../src/functions/string.js' ).sortVersions


describe( 'version', function(){
	it( 'sorts an array of versions', function(){
		const array		= [ 'bla-v5.4.32', 'bla-v1.0.0003', 'bla-v5.4.3', 'bla-v10.9.8', 'bla-v3.21.0', 'bla-v5.4.321' ]
		const expected	= [ 'bla-v1.0.0003', 'bla-v3.21.0', 'bla-v5.4.3', 'bla-v5.4.32', 'bla-v5.4.321', 'bla-v10.9.8' ]
		const sorted	= sortVersions( array )

		expect( sorted ).to.deep.equal( expected )
	})
	it( 'sorts and reverses an array of versions', function(){
		const array		= [ 'bla-v5.4.32', 'bla-v1.0.0003', 'bla-v5.4.3', 'bla-v10.9.8', 'bla-v3.21.0', 'bla-v5.4.321' ]
		const expected	= [ 'bla-v1.0.0003', 'bla-v3.21.0', 'bla-v5.4.3', 'bla-v5.4.32', 'bla-v5.4.321', 'bla-v10.9.8' ]
		const sorted	= sortVersions( array, true )

		expect( sorted ).to.deep.equal( expected.reverse() )
	})
})
