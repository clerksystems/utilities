'use strict'

const	{expect}							= require( 'chai' )
const	reqMod								= name => require( `../../src/functions/${name}.js` )


describe( 'forObject', function(){

	let forObject
	it( 'is requirable', function(){
		forObject = reqMod( 'object' ).forObject
		expect( forObject ).to.be.defined
		expect( forObject ).to.be.a( 'function' )
	})

	it( 'itterates over the keys and calls function ith value', function(){

		forObject(	{ '0': '0', '1': '1', '2': '2', '3': '3', '4': '4'}
				,	function( key, value ){
						expect( key ).to.equal( value )
					}
		)

	})

})
