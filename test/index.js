'use strict';

const	expect		= require( 'chai' ).expect
,		path		= require( 'path' )
,		fs			= require( 'fs' )
,		testDir		= path.join( __dirname, 'testdir' )
;

let		util		= null;




const test = name => require( `./modules/${name}-test.js` )

	test( 'forObject' )
	test( 'version' )




describe( 'the module', () =>
{
	it( 'should be requireable', () =>
	{
		util = require( '..' );
	});

	it ( 'should export a function called "objectSize" that counts object entries', () =>
	{
		expect( util.objectSize ).to.be.a( 'function' );
		expect( util.objectSize({ a:1, b:4 }) ).to.equal( 2 );
	});

	it ( 'should export over 20 functions', () =>
	{
		expect( util.objectSize( util ) ).to.be.above( 20 );
	});
});




describe( 'util.rmdirRecursivelySync', () =>
{
	it ( 'should remove a directory synchronously', ( done ) =>
	{
		util.createDirIfNotExistsSync( testDir );

		expect( fs.statSync( testDir ).isDirectory() ).to.be.true;

		util.rmdirRecursivelySync( testDir );

		try
		{
			expect( fs.statSync( testDir ).isDirectory() ).to.be.false;
			done();
		}
		catch ( err )
		{
			done();	// why is this ok?, there was an error....
		}
	});

});

describe( 'util.createDirIfNotExistsSyncRecursivly', () =>
{
	it ( 'should create a dir', () =>
	{
		util.createDirIfNotExistsSyncRecursivly( testDir );
		expect( util.dirExistsSync( testDir ) ).to.be.true;
	});
});
describe( 'util.createDirIfNotExists', () =>
{
	it ( 'should return a promise', ( done ) =>
	{
		const pr = util.createDirIfNotExists( testDir );
		expect( pr ).to.be.a('promise');
		pr.then( done );
	});

	// it.skip ( 'should create a new directory if one does not exist at given path', ( done ) =>
	// {

	// 	// util.createDirIfNotExists( path.join( __dirname, 'testdir' ) )
	// });
});



describe( 'util.forceArray', () =>
{

	it ( 'should array any value besides an array', () =>
	{
		expect( util.forceArray( 'henk' ) ).to.deep.equal( [ 'henk' ] );
		expect( util.forceArray( [ 1, 2 ] ) ).to.deep.equal( [ 1, 2 ] );
	});

});
