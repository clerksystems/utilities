describe( 'object functions', function()
{
	var path					= require( 'path' )
	,	lib 					= require( path.join( __dirname, '..', 'src', 'index' ) )
	,	expect					= require( 'chai' ).expect
	,	nonObjectTestValues 	= [ null, 6.4, false, 'hoi', [], '', -1, undefined ]
	;


	describe( 'surroTypeOf function', function()
	{
		it( 'should return "number" for Math.random()', function()
		{
			expect( lib.surroTypeOf( Math.random() ) ).to.equal( 'number' );
		});

		it( 'should return "null" for null', function()
		{
			expect( lib.surroTypeOf( null ) ).to.equal( 'null' );
		});

		it( 'should return "string" for "text"', function()
		{
			expect( lib.surroTypeOf( 'text' ) ).to.equal( 'string' );
		});

		it( 'should return "object" for {}', function()
		{
			expect( lib.surroTypeOf( {} ) ).to.equal( 'object' );
		});

		it( 'should return "array" for []', function()
		{
			expect( lib.surroTypeOf( [] ) ).to.equal( 'array' );
		});
	});


	describe( 'forceTemplate function', function()
	{
		it( 'should throw when the subject is not an object', function()
		{
			for ( var i = 0, l = nonObjectTestValues.length ; i < l ; i ++ )
			{
				try
				{
					lib.forceTemplate( testValues[ i ], {} );
					fail( nonObjectTestValues[ i ] + ' is not an object' );
				}
				catch ( err )
				{
					expect( typeof ( err ) ).to.equal( 'object' );
				}
			}
		});



		it( 'should throw when the template is not an object', function()
		{
			for ( var i = 0, l = nonObjectTestValues.length ; i < l ; i ++ )
			{
				try
				{
					lib.forceTemplate( {}, nonObjectTestValues[ i ] );
					fail( nonObjectTestValues[ i ] + ' is not an object' );
				}
				catch ( err )
				{
					expect( typeof ( err ) ).to.equal( 'object' );
				}
			}
		});


		it( 'should not throw when two objects are supplied', function()
		{
			try
			{
				lib.forceTemplate( {}, {} );
			}
			catch ( err )
			{
				fail( 'unnecessary throw' );
			}
		});




		it( 'should copy missing template properties over to the subject', function()
		{
			var subject = {}
			,	template = {
					'nr' : 1
				,	'str' : 'some text'
				,	'obj' : {}
				,	'obj2' : '$object'
				,	'arr' : []
				,	'arr2' : '$array'
				,	'n' : null
				,	'b' : true
				};

			subject = lib.forceTemplate( subject, template );

			expect( typeof( subject ) ).to.equal( 'object' );
			expect( subject.nr ).to.equal( template.nr );
			expect( subject.str ).to.equal( template.str );
			expect( subject.obj ).to.deep.equal( template.obj );
			expect( subject.obj2 ).to.deep.equal( {} );
			expect( subject.arr ).to.deep.equal( template.arr );
			expect( subject.arr2 ).to.deep.equal( [] );
			expect( subject.n ).to.equal( template.n );
			expect( subject.b ).to.equal( template.b );
		});


		it( 'should leave existing properties with the proper type alone', function()
		{
			var subject = {
					'nr' : 5
				,	'nr2' : 5
				,	'str' : 'some text'
				,	'str2' : 'some text'
				,	'obj' : { 'henk' : 8 }
				,	'obj2' : 'kaas'
				,	'obj3' : { 'test' : [] }
				,	'arr' : [ 1, 5, 51 ]
				,	'arr2' : {}
				,	'n' : 8
				,	'bool' : false
				,	'bool2' : 1
				,	'bool3' : 'true'
				,	'bool4' : 'true'
				}
			,	template = {
					'nr' : '$number'
				,	'nr2' : 10
				,	'str' : '$string'
				,	'str2' : 'some other text'
				,	'obj' : '$object'
				,	'obj2' : { 'piet' : 'hein' }
				,	'obj3' : { 'test' : {} }
				,	'arr' : '$array'
				,	'arr2' : []
				,	'n' : null
				,	'bool' : true
				,	'bool2' : true
				,	'bool3' : true
				,	'bool4' : '$boolean'
				};

			subject = lib.forceTemplate( subject, template );

			expect( typeof( subject ) ).to.equal( 'object' );
			expect( subject.nr ).to.equal( 5 );
			expect( subject.nr2 ).to.equal( 5 );
			expect( subject.str ).to.equal( 'some text' );
			expect( subject.str2 ).to.equal( 'some text' );
			expect( subject.obj.henk ).to.equal( 8 );
			expect( subject.obj2.piet ).to.equal( 'hein' );
			expect( subject.arr ).to.deep.equal( [ 1, 5, 51 ] );
			expect( subject.arr2 ).to.deep.equal( [] );
			expect( subject.n ).to.equal( template.n );
			expect( subject.bool ).to.equal( false );
			expect( subject.bool2 ).to.equal( true );
			expect( subject.bool3 ).to.equal( true );
			expect( subject.bool4 ).to.equal( true );
		});


		it( 'should check template object properties recursively', function()
		{
			var subject = {
					'obj' : {
							'obj' : 'kaas'
						,	'obj2' : {
								'henk' : 15
							,	'piet' : 27
							,	'kees' : 'hoi'
						}
					}
				}
			,	template = {
					'obj' : {
							'obj' : '$string'
						,	'obj2' : {
								'henk' : '$number'
							,	'piet' : 30
							,	'kees' : '$string'
						}
					}
				};

			subject = lib.forceTemplate( subject, template );

			expect( typeof( subject ) ).to.equal( 'object' );
			expect( subject.obj.obj ).to.equal( 'kaas' );
			expect( subject.obj.obj2.henk ).to.equal( 15 );
			expect( subject.obj.obj2.piet ).to.equal( 27 );
			expect( subject.obj.obj2.kees ).to.equal( 'hoi' );
		});



		it( 'should check subject arrays itemwise', function()
		{
			var subject = {
					'obj' : [
						{ 'name' : 'piet', 'age' : 15 }
					,	{ 'name' : 'klaas', 'age' : 23 }
					,	{ 'name' : 'henk', 'age' : 77, 'height' : 210, 'sex' : 'female' }
					]
				}
			,	template = {
					'obj' : [{
						'name' 		: '$string'
					,	'age' 		: '$number'
					,	'height' 	: '$number'
					,	'sex' 		: 'male'
					}]
				};

			subject = lib.forceTemplate( subject, template );

			expect( typeof( subject ) ).to.equal( 'object' );
			expect( subject.obj[ 0 ].name ).to.equal( 'piet' );
			expect( subject.obj[ 2 ].name ).to.equal( 'henk' );
			expect( subject.obj[ 1 ].height ).to.equal( 1 );
			expect( subject.obj[ 0 ].sex ).to.equal( 'male' );
			expect( subject.obj[ 2 ].sex ).to.equal( 'female' );
		});



		it( 'should not re-use the default object', function()
		{
			var subject = {}
			,	template = {
					'obj1' : {
						'ha' : 'string'
					,	'hi' : '$string'
					}
				,	'obj2' : {
						'ha' : '$string'
					}
				};

			subject = lib.forceTemplate( subject, template );

			expect( subject.obj1.ha ).to.equal( 'string' );
			expect( subject.obj1.hi ).to.equal( '' );
			expect( subject.obj2.ha ).to.equal( '' );
			expect( typeof( subject.obj2.hi ) ).to.equal( 'undefined' );
		});




		it( 'should throw errors when so required', function()
		{
			var subject1 = {
					'obj1' : {
						'ha' : 'hai'
					,	'hi' : 45
					}
				}
			,	subject2 = {
					'obj1' : {
						'ha' : 'hai'
					,	'hi' : 'yesy'
					}
				,	'obj2' : {
						'ha' : { 'bow' : [] }
					}
				}
			,	template = {
					'obj1' : {
						'ha' : '$string'
					,	'hi' : '$string'
					}
				,	'obj2' : {
						'ha' : { 'bow' : '$string' }
					}
				}
			;

			var error = null;

			try
			{
				subject = lib.forceTemplate( subject1, template, true );
			}
			catch( err )
			{
				// console.log( err );
				error = err;
			}

			expect( error.message ).to.equal( 'Template deviation, string expected @ obj1.hi' );

			try
			{
				subject = lib.forceTemplate( subject2, template, true );
			}
			catch( err )
			{
				// console.log( err );
				error = err;
			}
			expect( error.message ).to.equal( 'Template deviation, string expected @ obj2.ha.bow' );
		});

		it( 'should check for email values IF throwOnDeviation === true', function()
		{
			var subject = lib.forceTemplate( {}, { 'email' : '$email' } );
			expect( subject.email ).to.equal( '' );

			var error = null;

			try
			{
				lib.forceTemplate( {}, { 'email' : '$email' }, true )
			}
			catch( err )
			{
				error = err;
			}

			expect( error.message ).to.equal( 'Template deviation, email expected @ email' );
		});
	});



	describe( 'mergeObjects function', function()
	{
		it( 'should throw when obj1 or obj2 is not an object', function()
		{
			for ( var i = 0, l = nonObjectTestValues.length ; i < l ; i ++ )
			{
				try
				{
					lib.mergeObjects( {}, nonObjectTestValues[ i ] );
					fail( 'obj1 = ' + nonObjectTestValues[ i ] + ' is not an object' );

					lib.mergeObjects( nonObjectTestValues[ i ], {} );
					fail( 'obj2 = ' + nonObjectTestValues[ i ] + ' is not an object' );
				}
				catch ( err )
				{
					expect( typeof ( err ) ).to.equal( 'object' );
				}
			}
		});


		it( 'should not throw when two objects are supplied', function()
		{
			try
			{
				lib.mergeObjects( {}, {} );
			}
			catch ( err )
			{
				fail( 'unnecessary throw' );
			}
		});


		it ( 'should merge simple objects', function()
		{
			var mergedObj = lib.mergeObjects(
				{
					'a' : 10
				}
			,	{
					'b' : 'bon giorno'
				}
			);

			expect( mergedObj.a ).to.equal( 10 );
			expect( mergedObj.b ).to.equal( 'bon giorno' );
		});



		it ( 'should throw when presented with an array within an object', function()
		{
			try
			{
				var mergedObj = lib.mergeObjects(
					{
						'a' : 'ja'
					,	'b' : [ 1, 4 ]
					}
				,	{
						'b' : [ 4, 101, 'hoi daar' ]
					}
				);
				fail( 'missing error' )
			}
			catch ( err )
			{
				expect( typeof( err ) ).to.equal( 'object' );
			}
		});


		it( 'should merge nested objects', function()
		{
			var mergedObj = lib.mergeObjects(
				{
					'a' : 10
				,	'b' : { 'ba' : 56, 'bb' : 77 }
				}
			,	{
					'b' : { 'bc' : 88 }
				,	'c' : 'bon giorno'
				}
			);

			expect( mergedObj.a ).to.equal( 10 );
			expect( mergedObj.b.ba ).to.equal( 56 );
			expect( mergedObj.b.bb ).to.equal( 77 );
			expect( mergedObj.b.bc ).to.equal( 88 );
			expect( mergedObj.c ).to.equal( 'bon giorno' );
		});


		it( 'should prefer obj1 properties over obj2 properties in merge conflicts', function()
		{
			var mergedObj = lib.mergeObjects(
				{
					'a' : 10
				,	'b' : 10
				,	'c' : 'b'
				,	'd' : 'b'
				,	'e' : true
				,	'f' : { 'first' : 10, 'second' : 10 }
				,	'g' : { 'first' : 10, 'second' : 10 }
				,	'h' : 'doei'
				}
			,	{
					'a' : 5
				,	'b' : 15
				,	'c' : 'a'
				,	'd' : 'cornelis drebbel'
				,	'e' : 1
				,	'f' : { 'first' : 5, 'second' : 15, 'third' : 25 }
				,	'g' : 'hai'
				,	'h' : { 'henk' : 45 }
				}
			);

			expect( mergedObj.a ).to.equal( 10 );
			expect( mergedObj.b ).to.equal( 10 );
			expect( mergedObj.c ).to.equal( 'b' );
			expect( mergedObj.d ).to.equal( 'b' );
			expect( mergedObj.e ).to.equal( true );
			expect( mergedObj.f.first ).to.equal( 10 );
			expect( mergedObj.f.second ).to.equal( 10 );
			expect( mergedObj.f.third ).to.equal( 25 );
			expect( mergedObj.g.first ).to.equal( 10 );
			expect( mergedObj.g.second ).to.equal( 10 );
			expect( mergedObj.h ).to.equal( 'doei' );
		});








	})

});
