'use strict'

const util = require( './' )
const fs = require( 'fs' )
const path = require( 'path' );

(async function update (){
	let output = ''
	const add = text => output += text + '\n'

	const fileNames = await util.getFullContentPaths( './src/functions/' )
	fileNames.forEach( name => {
		add( name.split( path.sep ).slice(1).join( ' ' ) )
		const obj = require( './' + name )
		util.forObject( obj, itemName => add( '\t' + itemName ) )
	})

	console.log( output )
	output = '```\n' + output + '```'

	fs.writeFileSync( './readme.md', output )
})()
