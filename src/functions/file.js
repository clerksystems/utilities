'use strict';

// https://github.com/thenables/thenify-all

const 	_fs 					= require( 'fs' )
	,	os						= require( 'os' )

	, 	_path 					= require( 'path' )
	, 	_fnUtils				= require( './function' )
	, 	_arrUtils				= require( './array' )
;


let		_exports 				= module.exports = {}
;




_exports.readFile					= _fnUtils.promisify( _fs.readFile )
_exports.readDir					= _fnUtils.promisify( _fs.readdir )

_exports.isTrueDirEntry				= entry =>
										[ '.', '..' ].indexOf( entry ) === -1


_exports.filterActualFiles			= paths =>
										_fnUtils.promiseToMapAll( paths, _exports.fileExists )
										.then( exists => paths.filter( ( v, i ) => exists[ i ] === true ))

_exports.filterActualDirectories	= paths =>
										_fnUtils.promiseToMapAll( paths, _exports.directoryExists )
										.then( exists => paths.filter( ( v, i ) => exists[ i ] === true ))


_exports.getFullContentPaths		= dir =>
										_exports.readDir( dir )
										.then( contents =>
											contents
											.map( c =>
												_path.join( dir, c )
											)
										)


_exports.getExistingFiles				= paths =>
											_fnUtils.promiseToMapAll( paths, _exports.fileExists )
											.then( exists => paths.filter( ( v, i ) => exists[ i ] === true ) )
											.catch( err =>
											{
												log.error( `Error while getting existing files ${paths}: ${err}` )
											})

_exports.getExistingDirectories			= paths =>
											_fnUtils.promiseToMapAll( paths, _exports.directoryExists )
											.then( exists => paths.filter( ( v, i ) => exists[ i ] === true ) )
											.catch( err =>
											{
												log.error( `Error while getting existing files ${paths}: ${err}` )
											})

_exports.fileExistsSync = ( path ) =>
{
	try
	{
		return !! _fs.statSync( path ).isFile();
	}
	catch ( err )
	{
		return false;
	}
};


_exports.fileExists = ( path ) =>
{
	let		res = null
	let		rej = null
	const 	p	= new Promise( ( resolve, reject ) => { res = resolve, rej = reject } )
	try
	{
		_fs.stat( path, ( err, stat ) =>
		{
			if ( stat && stat.isFile() )
			{
				res( true )
			}
			else
			{
				res( false );
			}
		});
	}
	catch ( err )
	{
		res( false )
	}
	return p
}

_exports.dirExists = _exports.directoryExists = ( path ) =>
{
	let		res = null
	let		rej = null
	const 	p	= new Promise( ( resolve, reject ) => { res = resolve, rej = reject } )
	try
	{
		_fs.stat( path, ( err, stat ) =>
		{
			if ( stat && stat.isDirectory() )
			{
				res( true )
			}
			else
			{
				res( false );
			}
		});
	}
	catch ( err )
	{
		res( false )
	}
	return p
}

_exports.dirExistsSync =  ( path ) =>
{
	try
	{
		return _fs.statSync( path ).isDirectory();
	}
	catch ( err )
	{
		return false;
	}
};





_exports.readJsonFileIfExists = function( path )
{
	if ( !_exports.fileExistsSync( path ) )
	{
		return null;
	}

	let	dataObject 	= {}
	,	data 		= _fs.readFileSync( path, 'utf8' )
	;

	try
	{
		dataObject	= JSON.parse( data );
	}
	catch ( err )
	{
		return false;
	}

	return dataObject;
};





_exports.createFileIfNotExistsSync = function( path, chmod, content )
{
	content = content || '';

	try
	{
		var stats = _fs.statSync( path );

		if ( ! stats.isFile() )
		{
			throw new Error( 'Not a file!' );
		}
	}
	catch( exp )
	{
		_fs.writeFileSync( path, content );

		if ( chmod )
		{
			_fs.chmodSync( path, chmod );
		}
	}
};


_exports.createDirIfNotExistsSyncRecursivly = function( path, chmod )
{
	const replaceAll = (str, str1, str2, ignore) => str.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2)

	if( path.endsWith(_path.sep) )
		path = path.slice( 0, path.length -1 )

	if ( _exports.dirExistsSync( path ) === false )
	{
		let failed = false
		try {
			_fs.mkdirSync( path );
		} catch( err ){
			if( /ENOENT: no such file or directory, mkdir/.test( err ) ){
				failed = true
				if( os.platform() === 'win32' ){
					path = replaceAll( path, '/',  _path.sep )
				}

				const newPath = path
								.split( _path.sep )
								.reduce( (t,item,i,a) => {
									if( i !== a.length -1 ){
										t.push( item )
									}
									return t
								}, [])
								.join( _path.sep )
				if( newPath != '' ){
					_exports.createDirIfNotExistsSyncRecursivly( newPath, chmod )
				}
			} else {
				throw err
			}
		}

		if( failed ) _fs.mkdirSync( path );

		if ( chmod )
		{
			_fs.chmodSync( path, chmod );
		}
	}
};

_exports.createDirIfNotExistsSync = function( path, chmod )
{
	if ( _exports.dirExistsSync( path ) === false )
	{
		_fs.mkdirSync( path );

		if ( chmod )
		{
			_fs.chmodSync( path, chmod );
		}
	}
};


_exports.createDirIfNotExists = function( path, chmod, cb )
{
	let		res = null
	,		rej = null
	;

	const 	p	= new Promise( ( resolve, reject ) => { res = resolve, rej = reject } );

	if ( _exports.dirExistsSync( path ) === false )
	{
		_fs.mkdir( path, ( err ) =>
		{
			if ( err )
			{
				rej( err );
			}
			else if ( chmod )
			{
				_fs.chmod( path, chmod, ( err ) =>
				{
					if ( err )
					{
						rej( err );
					}
					else
					{
						res();
					}
				});
			}
			else
			{
				res();
			}
		});
	}
	else
	{
		res();
	}

	return p;
};









_exports.deleteFileIfExistsSync = ( path ) =>
{
	if( _exports.fileExistsSync( path ) )
	{
		_fs.unlinkSync( path );
	}
};



_exports.deleteFileIfExists = ( file ) =>
{
	let		res = null
	,		rej = null
	;

	const 	p	= new Promise( ( resolve, reject ) => { res = resolve, rej = reject } )

	_fs.unlink( file, err =>
	{
		err
			? rej( err )
			: res( true )
	})

	return p
};



_exports.rmdirRecursivelySync = ( dir ) =>
{
	var list = _fs.readdirSync(dir);
	for(var i = 0; i < list.length; i++) {
		var filename = _path.join(dir, list[i]);
		var stat = _fs.statSync(filename);

		if(filename == "." || filename == "..") {
			// pass these files
		} else if(stat.isDirectory()) {
			// rmdir recursively
			_exports.rmdirRecursivelySync(filename);
		} else {
			// rm fiilename
			_fs.unlinkSync(filename);
		}
	}
	_fs.rmdirSync(dir);
};



_exports.rmdirRecursively = ( dir ) =>
{
	const readDir = path => new Promise((res,rej) =>
		_fs.readdir( dir, ( err, list ) => {
			if( err ) rej( err )
			else res( list )
		})
	)
	const stat = path => new Promise( (res,rej) =>
		_fs.stat( path, (err, stats) => {
			if( err ) rej( err )
			else res( stats )
		})
	)
	const _rmdir = path => new Promise( (res, rej) =>
		_fs.rmdir( path, err => {
			if( err ) rej( err )
			else res()
		})
	)
	async function rmdir(){
		const list = await readDir( dir )
		for( let filename of list ){
			if( filename !== '.' && filename !== '..' ){
				const filePath = _path.join( dir, filename )
				const stats = await stat( filePath )
				if( stats.isDirectory() ){
					await _exports.rmdirRecursively( filePath )
				} else {
					await _exports.deleteFileIfExists( filePath )
				}
			}
		}
		await _rmdir( dir )
	}
};



_exports.emptyDirectory = function( path )
{
	_fs.readdirSync( path ).forEach( function( fileName )
	{
		if ( fileName !== '.' && fileName !== '..' )
		{
			_fs.unlinkSync( path + '/' + fileName );
		}
	});
};



_exports.humanizedFileSize = function( size_bt )
{
	var  size_kb = size_bt / 1000  // http://en.wikipedia.org/wiki/Kilobyte
		,size_mb = size_kb / 1000
		,size_gb = size_mb / 1000
		,size_tb = size_gb / 1000
	;

	var r1d = function( nr )
	{
		return Math.round( nr * 10 ) / 10;
	};

	if ( size_kb < 100 )
	{
		return r1d( size_kb ) + ' kB';
	}

	if ( size_mb < 100 )
	{
		return r1d( size_mb ) + ' mB';
	}

	if ( size_gb < 100 )
	{
		return r1d( size_gb ) + ' gB';
	}

	return r1d( size_tb ) + ' tB';
};



_exports.getFileExtension = ( fileName ) => ( fileName = fileName.substr( 1 + fileName.lastIndexOf( '/' ) ).split( '?' )[ 0 ]).substr( fileName.lastIndexOf( '.' ) + 1 );


_exports.getFileNameFromUrl = ( url ) => url.substring( url.lastIndexOf( '/' ) + 1 );



_exports.createDirStructure = function( initPath, structure )
{
	const str = Object.keys( structure );

	str
		.filter( i => typeof( structure[ i ] ) === 'string' )
		.forEach( i => _exports.createFileIfNotExistsSync( _path.join( initPath, i ), null, structure[ i ] ) )
	;

	str
		.filter( i => Object.prototype.toString.call( structure[ i ] ) === '[object Object]' )
		.forEach( i =>
		{
			_exports.createDirIfNotExistsSync( _path.join( initPath, i ) );

			_exports.createDirStructure( _path.join( initPath, i ), structure[ i ] )

		})
	;
};
