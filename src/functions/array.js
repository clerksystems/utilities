'use strict';

let _exports = module.exports = {};



_exports.flatten						= arr =>
											[].concat.apply( [], arr )



_exports.uniqueArray = function( array )
{
	var u = {}
	,	a = []
	;
	
	for ( var i = 0, l = array.length; i < l; ++i )
	{
		if ( u.hasOwnProperty( array[ i ] ) ) 
		{
			continue;
		}
		
		a.push( array[ i ] );
		
		u[ array[ i ] ] = 1;
	}
	
	return a;
};



_exports.isArray = ( o ) => Object.prototype.toString.call(o) === '[object Array]';


_exports.inArray = function( needle, haystack ) 
{
    for ( let i = 0, l = haystack.length; i < l; i++ ) 
	{
        if ( haystack[ i ] === needle )
		{
			return true;
		}
    }
	
    return false;
};




_exports.average = ( arr ) => arr.reduce( function(a, b) { return a + b; } ) / arr.length;



_exports.forceArray = ( subject ) => _exports.isArray( subject ) ? subject : [ subject ];