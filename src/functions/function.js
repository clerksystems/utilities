'use strict';


let _exports = module.exports = {};



_exports.promisify = fn =>
{
	return function( ...fnArgs )
	{
		return new Promise( ( resolve, reject ) =>
		{
			fn( ...fnArgs, function( err, result )
			{
				if ( err )
				{
					reject( err );
				}
				else
				{
					resolve( result );
				}
			});
		});
	};
};


_exports.promiseToFnAll	 			= filterFn =>
										( arr, filter ) =>
											Promise.all(
												Array.prototype[ filterFn ].call( arr , filter )
											)
_exports.promiseToMapAll			= ( arr, filter ) => _exports.promiseToFnAll( 'map' )( arr, filter )


_exports.callerName = function()
{
	var err = _exports.getErrorObject().stack.split( '\n' )
	;
	if ( err && err[ 5 ] )
	{
		return err[ 5 ];
	}
	else
	{
		return '';
	}
};


_exports.getErrorObject = function()
{
	try
	{
		throw Error('');
	}
	catch( err )
	{
		return err;
	}
};
