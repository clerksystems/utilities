'use strict';

let _exports = module.exports = {};



_exports.ucfirst = ( str ) => str.charAt( 0 ).toUpperCase() + str.substr( 1 );



_exports.isValidEmailAddress = ( testString ) =>
{
	const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return emailRegex.test( testString );
};



/**
 * @function sortVersions
 * @description sorts versions "<prefix><number>.<number>.<number>" and returns the sorted array
 * @param  {array} array		the array contianing the version strings
 * @param  {boolean} [reverse=false]	reverse() the result
 * @return {array}
 */
_exports.sortVersions = ( arr, reverse = false ) =>
{

	const regExp	= str => /\D+(\d+)[.](\d+)[.](\d+)/g.exec( str ).slice( 1 )
	const compare	= ( x, y ) => parseInt(x) - parseInt(y)

	const sorted = arr.sort( function( a, b ){
							a = regExp( a )
							b = regExp( b )

							const major = compare( a[0], b[0] )
							if( major !== 0 ) return major

							const minor = compare( a[1], b[1] )
							if( minor !== 0 ) return minor

							const build = compare( a[2], b[2] )
							if( build !== 0 ) return build

							return 0
						})
	if( reverse )	return sorted.reverse()
	return sorted
}
