'use strict';


let _exports = module.exports = {};


_exports.clone = function( src )
{
	function mixin(dest, source, copyFunc)
	{
		var  name
			,s
			,i
			,empty = {}
		;

		for ( name in source )
		{
			// the (!(name in empty) || empty[name] !== s) condition avoids copying properties in 'source'
			// inherited from Object.prototype.	 For example, if dest has a custom toString() method,
			// don't overwrite it with the toString() method that source inherited from Object.prototype
			s = source[name];

			if( ! ( name in dest ) || ( dest[name] !== s && ( ! ( name in empty ) || empty[name] !== s ) ) )
			{
				dest[name] = copyFunc ? copyFunc(s) : s;
			}
		}
		return dest;
	}

	if ( ! src || typeof src !== 'object' || Object.prototype.toString.call(src) === '[object Function]' )
	{
		return src;	// null, undefined, any non-object, or function
	}

	if ( src.nodeType && 'cloneNode' in src )
	{
		return src.cloneNode( true ); // DOM Node
	}

	if ( src instanceof Date )
	{
		return new Date( src.getTime() );	// Date
	}

	if ( src instanceof RegExp )
	{
		return new RegExp( src );   // RegExp
	}

	var  r
		,i
		,l
	;

	if ( src instanceof Array )
	{
		r = [];
		for ( i = 0, l = src.length; i < l; ++i )
		{
			if ( i in src )
			{
				r.push( _exports.clone( src[i] ) );
			}
		}
		// we don't clone functions for performance reasons
		//		} else if(d.isFunction(src)) {
		//			// function
		//			r = function(){ return src.apply(this, arguments); };
	}
	else
	{
		// generic objects
		r = src.constructor ? new src.constructor() : {};
	}
	return mixin( r, src, _exports.clone );
};




_exports.objectSize = function( obj )
{
    let size = 0, key;

    for ( key in obj )
	{
        if ( obj.hasOwnProperty( key ) )
		{
			size++;
		}
    }

    return size;
};




_exports.forceTemplate = function( subject, template, throwOnDeviation, path )
{
	if ( _exports.surroTypeOf ( subject ) !== 'object' )
	{
		throw new Error( 'Subject ' + subject + ' is not an object' );
	}

	if ( _exports.surroTypeOf ( template ) !== 'object' )
	{
		throw new Error( 'Template ' + template + ' is not an object' );
	}

	var templatePropertyType 	= null
	,	subjectPropertyType		= null
	,	deviationErrorMsg		= 'Template deviation'
	,	throwOnDeviation 		= !! throwOnDeviation
	,	path					= path || ''
	,	errorPath				= path
	,	defaults				= {
									'$string' 		: ''
								,	'$number' 		: 1
								,	'$function' 	: function(){}
								,	'$null' 		: null
								,	'$boolean'		: true
								,	'$array'		: []
								,	'$object'		: {}
								}
	,	regexes					= {
									'$email' 		: {
														'regex' 	: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
													,	'default'	: ''
													}
								}
	;

	for ( var i in template )
	{
		templatePropertyType 	= _exports.surroTypeOf( template[ i ] );
		subjectPropertyType 	= _exports.surroTypeOf( subject[ i ] );

		errorPath = ( path ? path + '.' : '' ) + i;

		// console.log( errorPath );
		// console.log( i, templatePropertyType, subjectPropertyType );

		switch ( templatePropertyType )
		{
			case 'array':
				if ( subjectPropertyType !== 'array' )
				{
					if ( throwOnDeviation  )
					{
						throw new Error( deviationErrorMsg + ', array expected @ ' + errorPath );
					}
					subject[ i ] = defaults.$array;
				}


				if ( typeof( template[ i ][ 0 ] ) !== 'undefined' )
				{
					for ( var arrayIndex = 0, l = subject[ i ].length ; arrayIndex < l ; arrayIndex ++ )
					{
						subject[ i ][ arrayIndex ] = _exports.forceTemplate( subject[ i ][ arrayIndex ], template[ i ][ 0 ], throwOnDeviation, errorPath );
					}
				}
				break;


			case 'object':
				if ( subjectPropertyType !== 'object' )
				{
					if ( throwOnDeviation  )
					{
						throw new Error( deviationErrorMsg + ', object expected @ ' + errorPath );
					}
					subject[ i ] = _exports.clone( defaults[ '$object' ] );
				}

				subject[ i ] = _exports.forceTemplate( subject[ i ], template[ i ], throwOnDeviation , errorPath );
				break;

			case 'null':
			default:
				// console.log( 'defaults[ ' + template[ i ] + ' ] = ' + defaults[ template[ i ] ] );

				if ( typeof( defaults[ template[ i ] ] ) !== 'undefined' )
				{
					// a $type was defined

					if ( '$' + subjectPropertyType !==  template[ i ] )
					{
						if ( throwOnDeviation  )
						{
							throw new Error( deviationErrorMsg + ', ' + template[ i ].substr( 1 ) + ' expected @ ' + errorPath );
						}
						subject[ i ] = defaults[ template[ i ] ];

					}

				}
				else if ( typeof( regexes[ template[ i ] ] ) !== 'undefined' )
				{
					// a $regex was defined
					if ( regexes[ template[ i ] ].regex.test( subject[ i ] ) !== true )
					{
						if ( throwOnDeviation )
						{
							throw new Error( deviationErrorMsg + ', email expected @ ' + errorPath );
						}
						subject[ i ] = regexes[ template[ i ] ].default;
					}

				}
				else
				{
					if ( subjectPropertyType !== templatePropertyType )
					{
						if ( throwOnDeviation  )
						{
							throw new Error( deviationErrorMsg + ', ' + templatePropertyType + ' expected @ ' + errorPath );
						}
						subject[ i ] = template[ i ];
					}
					else
					{
						// do nothing
					}
				}
				break;
		}
	}

	return subject;
};




_exports.mergeObjects = function( obj1, obj2 )
{
	if ( _exports.surroTypeOf ( obj1 ) !== 'object' )
	{
		throw new Error( 'obj1 ' + obj1 + ' is not an object' );
	}

	if ( _exports.surroTypeOf ( obj2 ) !== 'object' )
	{
		throw new Error( 'obj2 ' + obj2 + ' is not an object' );
	}

	var mergedObject = _exports.clone( obj1 );

	for ( var i in obj2 )
	{
		if ( _exports.surroTypeOf( obj2[ i ] ) === 'array' )
		{
			throw new Error( 'Cannot merge arrays - obj2 ' + i + ' is an array' );
		}

		if ( obj2.hasOwnProperty( i ) )
		{
			switch ( _exports.surroTypeOf( obj1[ i ] ) )
			{
				case 'array':
					throw new Error( 'Cannot merge arrays - obj1 ' + i + ' is an array' );


				case 'undefined':
					// obj1 does not have this value yet
					mergedObject[ i ] = obj2[ i ];
					break;

				case 'object':
					if ( _exports.surroTypeOf( obj2[ i ] ) === 'object' )
					{
						mergedObject[ i ] = _exports.mergeObjects( obj1[ i ], obj2[ i ] );
					}
					else
					{
						mergedObject[ i ] = obj1[ i ];
					}
					break;

				default:
					// leave original value
					break;
			}
		}
	}

	return mergedObject;
};



_exports.identicalKeys = function( obj1, obj2 )
{
	if ( _exports.objectSize( obj1 ) !== _exports.objectSize( obj2 ) )
	{
		return false;
	}
	else
	{
		for ( var i in obj1 )
		{
			if ( typeof( obj2[ i ] ) === 'undefined' )
			{
				return false;
			}
		}

		return true;
	}
};


/**
 * @function typeOf
 * @description returns a string that describes the type, returns a string if no 2nd arg else a boolean
 * @param  {any}	item	item to check
 * @param  {string}	[type]	type to check
 * @return {String|Boolean}
 */
_exports.typeOf = function( item, type ){
	const t = Object.prototype.toString.call( item ).slice( 8, -1 ).toLowerCase()
	return type === undefined ? t : t === type
}


_exports.surroTypeOf = function( testVar )
{
	var type = typeof( testVar );

	if ( type !== 'object' )
	{
		return type;
	}
	else
	{
		if ( require( './array' ).isArray( testVar ) )
		{
			return 'array';
		}
		else if ( testVar === null )
		{
			return 'null'
		}
		return 'object';
	}
};



_exports.findEntry = function( entryString, obj, newValue )
{
	try
	{
		splitEntryString = entryString.split( '.' );

		var entry = obj;

		for ( var i = 0, l = splitEntryString.length ; i < l ; i ++ )
		{
			var step = splitEntryString[ i ];

			if ( typeof( entry[ step ] ) === 'undefined' )
			{
				return global.log.error( 'Could not find entry: "' + entryString + '", error on: "' + step + '"' );
			}

			if ( step === splitEntryString[ splitEntryString.length ] && typeof( newValue ) !== 'undefined' )
			{
				entry[ step ] = newValue;
			}

			entry = entry[ step ];
		}
	}
	catch( exp )
	{
		globals.log.error( exp.message );
	}


	return entry;
};





_exports.sortKeys = function( obj )
{
	var sortedKeys = Object.keys( obj ).sort( function( a, b )
	{
		return a > b
	});

	return sortedKeys;
};





_exports.getKeys = function( obj )
{
   var keys = [];
   for ( var key in obj )
   {
      keys.push( key );
   }
   return keys;
};




_exports.addGetter = function( obj, name, callback )
{
	Object.defineProperty( obj, name, callback );
	return obj;
};




_exports.forObject = function( obj, fn ){
	Object
	.keys( obj )
	.forEach( function( name ){
		const item = obj[name]
		fn( name, item )
	})
}
