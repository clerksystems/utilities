'use strict';

const	_fs			= require( 'fs' )
,		_path		= require( 'path' )
;

let		_exports	= {};

_fs.readdirSync( _path.join( __dirname, 'functions' ) ).forEach( ( fileName ) => 
{
	if ( fileName !== '.' && fileName !== '..' )
	{
		let functionObject = require( _path.join( __dirname, 'functions', fileName ) );
		
		for ( let f in functionObject )
		{
			if ( functionObject.hasOwnProperty( f ) )
			{
				if ( typeof( _exports[ f ] ) !== 'undefined' )
				{
					throw new Error( 'Cannot add util function ' + f + ' from ' + fileName + 'twice' );
				}
				
				Object.defineProperty( _exports, f, 
				{
					get 		: () => functionObject[ f ]
				,	enumerable	: true
				});
				
			}
		}
	}
});

module.exports = _exports;